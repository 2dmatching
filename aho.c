#define DRIVER 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "code.h"

struct kword {
	unsigned char *word;
	struct kword *next;
};

#define MAXCHAR 128
//#define MAXCHAR 260
//#define MAXCHAR 1030

static int MaxState;

static int *MatchArray;

#define MULTI_WAY -1
#define EMPTY_SLOT -2

union GotoTable {
	int GotoState;
	int *BranchTable;
} static *GotoArray;

#define FAIL_STATE -1

static struct kword **OutArray;

static int *FailArray;

static int HighState;

static void AddStateTrans ( int, int ,int );
static void ComputeFail ( void );
static void Enter ( unsigned char * );
static void FindFail ( int state, int s, int a );
static void QueueAdd ( int *queue, int qbeg, int new );

int RetrieveChar ( char **text );

int textcolumn = 0;

void MsrchInit ( struct kword *klist )
{
	int i;
	
	struct kword *ktemp;
	
	MaxState = 1;
	
	/* MaxState equals the number of characters */
	for ( ktemp = klist; ktemp != NULL; ktemp = ktemp->next)
		MaxState += strlen( ktemp->word );
	
	MatchArray = (int *) malloc ( sizeof(int) * MaxState );
	GotoArray = (union GotoTable *)  malloc (sizeof (union GotoTable) * MaxState );
	OutArray = (struct kword **)  malloc (sizeof (struct kword *) * MaxState );
	FailArray = (int *)  malloc (sizeof (int) * MaxState );
	
	for ( i = 0; i < MaxState; i++) {
		MatchArray[i] = EMPTY_SLOT;
		OutArray[i]   = NULL;
		}
	
	HighState = 0;
	AddStateTrans ( 0, 'a', FAIL_STATE );
	AddStateTrans ( 0, 'b', FAIL_STATE );
	
	for ( ; klist != NULL; klist = klist->next )
		Enter ( klist->word );
	
	for ( i = 0; i < MAXCHAR; i++)
		if ( GotoArray[0].BranchTable[i] == FAIL_STATE )
			GotoArray[0].BranchTable[i] = 0;

	ComputeFail();
}

/* Add transition from OldState to NewState for MatchChar */
static void AddStateTrans ( int OldState, int MatchChar, int NewState )
{
	int i, *temp;
	
	if ( MatchArray[OldState] == EMPTY_SLOT ) {
		MatchArray[OldState] = MatchChar;
		GotoArray[OldState].GotoState = NewState;
	}
	
	else
		if ( MatchArray[OldState] == MULTI_WAY )
			GotoArray[OldState].BranchTable[MatchChar] = NewState;
	
	else {
		
		temp = (int *) malloc ( sizeof(int) * MAXCHAR );
		
		for ( i = 0; i < MAXCHAR; i++ )
			temp[i] = FAIL_STATE;

	
		temp[MatchArray[OldState]] = GotoArray[OldState].GotoState;
	
		temp[MatchChar] = NewState;
	
		MatchArray[OldState] = MULTI_WAY;
		GotoArray[OldState].BranchTable = temp;
		}
}

/* Add word to the list of words the program recognises */
static void Enter ( unsigned char *kword )
{
	int state, k;
	char *save;
	struct kword *ktemp;
	
	state = 0;
	
	/* keep a copy */
	save = kword; 
	
	for ( ; *kword != '\0'; kword++ ) {
		if ( MatchArray[state] == *kword )
			state = GotoArray[state].GotoState;
		
		else
			if ( MatchArray[state] == MULTI_WAY ) {
				if (( k = GotoArray[state].BranchTable[*kword] ) == FAIL_STATE)
					break;
				else
					state = k;
			}
			else
				break;
	}
	
	for ( ; *kword != '\0'; kword++ ) {
		HighState += 1;
		
		if ( HighState >= MaxState) {
			printf("Too many states!\n");
			exit(EXIT_FAILURE);
		}
	
		AddStateTrans ( state, *kword, HighState );
		state = HighState;
	}
	
	ktemp = (struct kword *) malloc ( sizeof ( struct kword ));
	ktemp->word = save;
	ktemp->next = OutArray[state];
	OutArray[state] = ktemp;
}

static void ComputeFail()
{
	int *queue, qbeg, r, s;
	int i;
	
	/* Allocate a queue */
	queue = (int *) malloc ( sizeof( int ) * MaxState);
	qbeg = 0;
	queue[0] = 0;
	
	for ( i = 0; i < MAXCHAR; i++ )
		if (( s = GotoArray[0].BranchTable[i] ) != 0 ) {
			FailArray[s] = 0;
			QueueAdd ( queue, qbeg, s );
		}

	while ( queue[qbeg] != 0)
	{
		r = queue[qbeg];
		qbeg = r;
	
		if ( MatchArray[r] == EMPTY_SLOT )
			continue;
		
		else
			if ( MatchArray[r] == MULTI_WAY ) {
				for ( i = 0; i < MAXCHAR; i++ )
					if (( s = GotoArray[r].BranchTable[i] ) != FAIL_STATE ) {
						QueueAdd ( queue, qbeg, s );
						FindFail ( FailArray[r], s, i);
					}
			}
		else {
			QueueAdd ( queue, qbeg, GotoArray[r].GotoState );
			FindFail ( FailArray[r], GotoArray[r].GotoState, MatchArray[r] );
		}
	}
	free ( queue );
}

	
static void FindFail ( int s1, int s2, int a )
{
	int on_fail;
	struct kword *ktemp, kdummy, *out_copy, *kscan;
	
	for ( ; ; s1 = FailArray[s1] )
		if ( MatchArray[s1] == a ) {
			if (( on_fail = GotoArray[s1].GotoState ) != FAIL_STATE )
				break;
		}
		else
			if ( MatchArray[s1] == MULTI_WAY )
				if (( on_fail = GotoArray[s1].BranchTable[a] ) != FAIL_STATE )
					break;

	FailArray[s2] = on_fail;
	
	if( OutArray[on_fail] == NULL )
		out_copy = NULL;
	else {
	
		kscan = OutArray[on_fail];
		out_copy = malloc ( sizeof (struct kword ));
		out_copy->word = kscan->word;
		out_copy->next = NULL;
	
		for ( kscan = kscan->next; kscan != NULL; kscan = kscan->next ) {

			ktemp = malloc ( sizeof ( struct kword ));
			ktemp->word =  kscan->word;
			ktemp->next = out_copy->next;
			out_copy->next = ktemp;
		}
	}
	
	if (( kdummy.next = OutArray[s2] ) != NULL ) {
		ktemp = &kdummy;
		for ( ; ktemp->next->next != NULL; ktemp = ktemp->next )
			;
		ktemp->next->next = out_copy;
	}
	
	else
		OutArray[s2] = out_copy;
}
	
static void QueueAdd ( int *queue, int qbeg, int new ) 
{
	int q;
	
	q = queue[qbeg];
	
	if ( q == 0 )
		queue[qbeg] = new;
	else {
		for ( ; queue[q] != 0; q = queue[q] )
			;
		queue[q] = new;
	}
	
	queue[new] = 0;
}

/* Do the actual search */
int MsrchGo ( char **text )
{
	int state, c, g, mm;
	struct kword *kscan;
	
	state = 0;
	
	while (( c = RetrieveChar( text ) ) != EOF ) {
		
		for ( ;; ) {
					
			if ( state == 0 || ( mm = MatchArray[state] ) == MULTI_WAY )
				g = GotoArray[state].BranchTable[c];

			else 
				if ( mm == c )
					g = GotoArray[state].GotoState;

			else
				g = FAIL_STATE;
			
			if ( g != FAIL_STATE )
				break;
			
			state = FailArray[state];
		}
		state = g;

		/* Output results */
		if (( kscan = OutArray[state] ) != NULL )
			for ( ; kscan != NULL; kscan = kscan->next )
				/* Break on the first result */
				return (textcolumn - strlen(kscan->word));
	}
	return -1;
}
	
void MsrchEnd ( void )
{
	int i;
	
	struct kword *kscan;
	
	for ( i = 0; i < MaxState; i++ )
		if ( MatchArray[i] == MULTI_WAY )
			free ( GotoArray[i].BranchTable );
		
	free ( MatchArray );
	free ( GotoArray );
	free ( FailArray );
	
	for ( i = 0; i < MaxState; i++ )
		if ( OutArray[i] != NULL )
			for ( kscan = OutArray[i]; kscan != NULL; kscan = kscan->next )
				free ( kscan );
	
	free ( OutArray );
}


#ifdef DRIVER

#define BUFSIZE 200

FILE *infile;
char inbuf[BUFSIZE];
char *inbufptr;
int linetextcolumn;

int textrow;
int textlength;

int aho( char **text, char **pattern, int m, int n, int trow, int tcolumn, int prow, int tlength )
{
	textrow = trow;
	textlength = tlength;
	textcolumn = tcolumn;
	
	struct kword *khead, *ktemp;
	
	int i;
	
	linetextcolumn = 0;
	inbufptr = NULL;
	
	khead = NULL;
	
	char *pattern_small;
	
	pattern_small = malloc (m + 1);
	
	for( i = 0; i < m; i++)
		pattern_small[i] = pattern[prow][i];
	
	ktemp = ( struct kword * ) malloc ( sizeof ( struct kword ));
	ktemp->word = pattern_small;
	ktemp->next = khead;
	
	if (khead != 0 && khead != ktemp)
		if (khead->word == ktemp->word)
			printf("Words point to the same thing - this is not valid\n");

	khead = ktemp;

	/* Setup system and pass list of words */
	MsrchInit ( khead );
	
	/* Now search */
	int result = MsrchGo( text );
	
	/* Do a clean up */
	MsrchEnd();
	
	free ( pattern_small );

	return ( result );
}

int RetrieveChar( char **text )
{
	if ( textcolumn < textlength ) {
		textcolumn ++;
		return text[textrow][textcolumn - 1];
	}
	
	else
		return ( EOF );
}

#endif
