#include "code.h"

int kmpNext[128];

void prep_kmp( double *text2, double *pattern2, int m, int n )
{
	int i, j;

	i = 0;
	j = kmpNext[0] = -1;

	while (i < m) {
		while (j > -1 && pattern2[i] != pattern2[j])
		        j = kmpNext[j];

	        i++;
	        j++;

	        if (pattern2[i] == pattern2[j])
		         kmpNext[i] = kmpNext[j];
	        else
	  	      kmpNext[i] = j;
	}
}

unsigned int search_kmp( double *text2, double *pattern2, int m, int n )
{
	prep_kmp( text2, pattern2, m, n );

	unsigned int i = 0, j = 0, matches = 0;

	#define limit n * n - (n * ( m - 1 ))
	
	while (i < limit) {
		while (j > -1 && pattern2[j] != text2[i])
			j = kmpNext[j];
			
		i++;
		j++;
			
		if (j == m) {
			matches++;
			j = kmpNext[j];
		}
	}		
	return matches;
}

