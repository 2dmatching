CC = gcc -Wall -O2 -funroll-loops
#CC = gcc -Wall -g

TARGET = code

OBJS= naive.o karp.o code.o helper.o zhu.o bird.o bird2.o aho.o baeza.o baeza2.o bm.o kmp.o

LDFLAGS=-lmpi

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC)  $(OBJS) -o $(TARGET) $(LDFLAGS)
	
naive.o: naive.c
	$(CC) -c naive.c

karp.o: karp.c
	$(CC) -c karp.c

code.o: code.c
	$(CC) -c code.c

helper.o: helper.c
	$(CC) -c helper.c

zhu.o: zhu.c
	$(CC) -c zhu.c

bird.o: bird.c
	$(CC) -c bird.c
	
bird2.o: bird2.c
	$(CC) -c bird2.c

aho.o: aho.c
	$(CC) -c aho.c
	
baeza.o: baeza.c
	$(CC) -c baeza.c
	
baeza2.o: baeza2.c
	$(CC) -c baeza2.c
	
bm.o: bm.c
	$(CC) -c bm.c
	
kmp.o: kmp.c
	$(CC) -c kmp.c
	
clean:
	rm -f *.o $(TARGET) core *.txt

