#include "code.h"

int naive2( char **text, char **pattern, int m, int n, int textrow, int textcolumn, int patternrow )
{
	unsigned int i, j, k;
	
	/* Slide the text's window */
	for ( i = textcolumn; i < n - m + 1; i++ ) {
	
		k = i;

		/* Start searching against the first pattern character */
		for( j = 0; j < m; j++ ) {

			if( pattern[patternrow][j] == text[textrow][k] )
				k++;
				
			else
				break;
		}
		/* If j finished without breaking return success */
		if ( j == m )
			return k-m;
		else
			j = 0;
	}
	/* No match found on this row */
	return -1;
}

int naive2b( char **text, char **pattern, int m, int n, int textrow, int textcolumn, int patternrow )
{
	unsigned int j;
	
	/* Search only m characters after the current text's column without sliding the window */
	for ( j = 0; j < m; j++ ) {
		
		if( pattern[patternrow][j] == text[textrow][textcolumn] )
			textcolumn++;
			
		else
			return -1;
	}
	/* If reached through here, report success */
	return 0;
}

unsigned int baeza2( char **text, char **pattern, int m, int n )
{
	int x, i, j, row, matches = 0;
	
	/* Iterate every m rows of the text (first check lines 0 to m - 1) */
	for ( row = m - 1; row < n; row += m ) {

		/* Search each row against all m lines of the pattern */
		for ( i = 0; i < m; i++ ) {

			/* In each row start searching from the begining */
			x = 0;
		
			while ( x != -1) {

				/* Check text's row against pattern's row */
				x = naive2( text, pattern, m, n, row, x, i);

				/* If a row is not found procceed to the next pattern's row */
				if ( x != -1 ) {	

					/* If there is a match at i = 3, check vertically the text above for i = 0-2 and below for 4-m*/
							
					/* Check lines above the current */
					for ( j = 0; j < i; j++ )
						if( naive2b( text, pattern, m, n, row - i + j, x, j ) == -1)
							goto newline;
						
					/* Check lines below the current */
					for ( j = 1; j < m - i; j++ ) {

						/* Avoid crashing when x = n or n-1 */
						if ( row + j >= n )
							goto newline;
						
						if ( naive2b( text, pattern, m, n, row + j, x, i + j ) == -1)
							goto newline;
					}
			
					/* If we reached here we should have a match */
					matches++;

				/* If a mismatch is found, don't bother searching for the other y lines */
				newline:

					/* Now increase counter to continue searching on the same row */
					x++;
				}
				else break;
			}
		}
	}
	return matches;
}
