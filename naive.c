#include "code.h"

unsigned int naive( char **text, char **pattern, int m, int n )
{
	unsigned int i, j, k, l;
	unsigned int matches = 0;
	unsigned int x, y;
	
	/* Text size 7, pattern size 3, so the last element would be 7 - 3 + 1 = 5 */
	for (y = 0; y < n - m + 1; y++) {

		for (x = 0; x < n - m + 1; x++) {

			k = y; /* reset text's vertical k (not to 0 but to the number of vertical loops) */

			for (l = 0; l < m; l++) {
			
				i = x; /* reset text's horizontal i (not to 0 but to the number of horizontal loops) */

				for (j = 0; j < m; j++) {
					
					if ( pattern[l][j] != text[k][i] )
						goto newline;

					i++; /*increase horizontal drift*/
				}
		
				k++; /*increase vertical drift*/
			}

			/* If we reached here we should have a match */
			matches++;
			
		newline: ;
		}
	}	
	return matches;
}
