#include "code.h"

int main(int argc, char *argv[])
{
	double t1, t2, t3 = 0;

	int i;
	
	int times, m, n;
	
	if( argc != 4) {
		printf("Please provide pattern, text and loop sizes!\n");
		exit(1);
	}
	else {
		m = atoi(argv[1]);
		n = atoi(argv[2]);
		times = atoi(argv[3]);
	}
	
	MPI_Init( &argc, &argv );
	
	//print_pattern();
	
	/* Allocate text and pattern */
	char **text = ( char ** )malloc( n * sizeof( char * ));
	
	if( text == NULL ) {
		printf("Failed to allocate array!\n");
		exit(1);
	}
	
	for( i = 0; i < n; i++ ) {
		text[i] = ( char * )malloc( n * sizeof( char ));
		
		if( text[i] == NULL ) {
			printf("Failed to allocate array!\n");
			exit(1);
		}	
	}
	
	char **pattern = ( char ** )malloc( m * sizeof( char * ));

	if( pattern == NULL ) {
		printf("Failed to allocate array!\n");
		exit(1);
	}
	
	for( i = 0; i < m; i++ ) {
		pattern[i] = ( char * )malloc( m * sizeof( char ));
		
		if( pattern[i] == NULL ) {
			printf("Failed to allocate array!\n");
			exit(1);
		}	
	}
	
	printf("Executing all algorithms %i times for m = %i and n = %i\n",times, m, n);

	for (i = 0; i < times; i++) {
		create_files( m , n, ALPHABET_SIZE ); 
		load_files( text, pattern, m, n );
		t1 = MPI_Wtime();
		naive( text, pattern, m, n );
		t2 = MPI_Wtime();
		t3 += t2 - t1;
	}

	printf("Average elapsed time for naive: %f\n", t3/times);
	
	t3 = 0;

	for (i = 0; i < times; i++) {
		create_files( m , n, ALPHABET_SIZE ); 
		load_files( text, pattern, m, n );
		t1 = MPI_Wtime();
		karp( text, pattern, m, n );
		t2 = MPI_Wtime();
		t3 += t2 - t1;
	}

	printf("Average elapsed time for karp: %f\n", t3/times);
	
	t3 = 0;

	for (i = 0; i < times; i++) {
		create_files( m , n, ALPHABET_SIZE ); 
		load_files( text, pattern, m, n );
		t1 = MPI_Wtime();
		zhu( text, pattern, m, n, 1 );
		t2 = MPI_Wtime();
		t3 += t2 - t1;
	}

	printf("Average elapsed time for zhu1: %f\n", t3/times);
	
	t3 = 0;

	for (i = 0; i < times; i++) {
		create_files( m , n, ALPHABET_SIZE ); 
		load_files( text, pattern, m, n );
		t1 = MPI_Wtime();
		zhu( text, pattern, m, n, 2 );
		t2 = MPI_Wtime();
		t3 += t2 - t1;
	}

	printf("Average elapsed time for zhu2: %f\n", t3/times);
	
	t3 = 0;
	
	for (i = 0; i < times; i++) {
		create_files( m , n, ALPHABET_SIZE ); 
		load_files( text, pattern, m, n );
		t1 = MPI_Wtime();
		bird( text, pattern, m, n );
		t2 = MPI_Wtime();
		t3 += t2 - t1;
	}

	printf("Average elapsed time for bird1: %f\n", t3/times);
	
	t3 = 0;

	for (i = 0; i < times; i++) {
		create_files( m , n, ALPHABET_SIZE ); 
		load_files( text, pattern, m, n );
		t1 = MPI_Wtime();
		bird2( text, pattern, m, n );
		t2 = MPI_Wtime();
		t3 += t2 - t1;
	}

	printf("Average elapsed time for bird2: %f\n", t3/times);
	
	t3 = 0;

	for (i = 0; i < times; i++) {
		create_files( m , n, ALPHABET_SIZE ); 
		load_files( text, pattern, m, n );
		t1 = MPI_Wtime();
		baeza( text, pattern, m, n );
		t2 = MPI_Wtime();
		t3 += t2 - t1;
	}

	printf("Average elapsed time for baeza1: %f\n", t3/times);
	
	t3 = 0;

	for (i = 0; i < times; i++) {
		create_files( m , n, ALPHABET_SIZE ); 
		load_files( text, pattern, m, n );
		t1 = MPI_Wtime();
		baeza2( text, pattern, m, n );
		t2 = MPI_Wtime();
		t3 += t2 - t1;
	}

	printf("Average elapsed time for baeza2: %f\n", t3/times);
	
	t3 = 0;
	
	MPI_Finalize();

	//Deallocate text and pattern
	for( i = 0; i < n; i++)
		free( text[i] );
		
	free( text );
	
	for( i = 0; i < m; i++)
		free( pattern[i] );
		
	free( pattern );
	
	return 0;
}

