#include "code.h"

unsigned int zhu( char **text, char **pattern, int m, int n, int version )
{
	/* Huge optimization instead of unsigned int B */
	#define B 128

	double *text2 = ( double* ) malloc( n * n * sizeof( double ));
	
	if( text2 == NULL ) {
		printf("Failed to allocate array!\n");
		exit(1);
	}
	
	double *pattern2 = ( double* ) malloc( m * sizeof( double ));
	
	if( pattern2 == NULL ) {
		printf("Failed to allocate array!\n");
		exit(1);
	}
	
	unsigned int i, j, k, l = 0, matches;
	
 	/* Reduce text to 1d and calculate hash */
	for( k = 0; k < n - m + 1; k++ )
		for( j = 0; j < n; j++, l++ ) {

			text2[l] = 0;

			for( i = 0 + k; i < m + k; i++ )
				text2[l] = text2[l] * B + text[i][j] % Q;
		}

	/* Reduce pattern to 1d and calculate hash */
	for( j = 0; j < m; j++ ) {

		pattern2[j] = 0;

		for( i = 0; i < m; i++ )
			pattern2[j] = pattern2[j] * B + pattern[i][j] % Q;
	}

	if ( version == 1 )
		matches = search_kmp( text2, pattern2, m, n );
		
	else
		matches = search_bm( text2, pattern2, m, n );
		
	free(text2);
	free(pattern2);
		
	return matches;
}
