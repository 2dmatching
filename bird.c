#include "code.h"

unsigned int bird( char **text, char **pattern, int m, int n )
{	
	/* x and y can be -1 */
	int x, y;
	
	unsigned int i, row, matches = 0;
	
	/* Iterate rows */
	for ( row = 0; row < n - m + 1; row++ ) {
	
		x = 0;
		
		/* Iterate through results on the same row */
		while ( x != -1 ) {

			x = aho( text, pattern, m, n, row, x, 0, n - m + 1 );
			
			/* If a row is found, check the m - 1 rows underneath it for matches */
			if ( x != -1 ) {

				for ( i = 1; i < m; i++) {
					/* Optimization: instead of checking against n chars on
					 subsequent rows, just check against the next m chars after x */
 					y = aho( text, pattern, m, n, row + i, x, i, x + m );
 					
					if ( y != x )
						break;
						
					/* If we reached to the final line above x without breaking, we have a match */
					else if ( i == m -1)
						matches++; 
				}
				/* After each attempt,increase the counter + 1 */
				x++;
			}
		}
	}
	return matches;
}
