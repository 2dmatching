/*A list of helper functions*/

#include "code.h"
#include <time.h>

/*Prints text and pattern*/
void print_pattern( char **text, char **pattern, int m, int n )
{
	unsigned int i,j;

	/*Prints the pattern*/
	for( j = 0; j < m; j++ ) {
		for( i = 0; i < m; i++ )
			printf("%i",pattern[j][i]);
			
		printf("\n");
	}

	/*Prints the text*/
	for( j = 0; j < n; j++ ) {
		for( i = 0; i < n; i++ )
			printf("%i",text[j][i]);
			
		printf("\n");
	}
}

/*Returns the square of a number*/
int power(int number, int power)
{
	int i, returned_number = number;

	for( i = 1; i < power; i++ )
		returned_number = returned_number * number;
	
	return returned_number;
}

/*Loads text and pattern into tables*/
void load_files( char **text, char **pattern, int m, int n )
{
	FILE *text_fp, *pattern_fp;
	
	unsigned int i,j;

	int c;
	
	/*Pattern loading*/
	pattern_fp = fopen("data/pattern", "r");
	
	for(j = 0; j < m; j++)
		for(i = 0; i < m; i++) {
			c = fgetc( pattern_fp );
			/* If we encounter '\n' pull the next character*/
			if( c != 10 )
				pattern[j][i] = c;
			else
				pattern[j][i] = fgetc( pattern_fp );
		}
		
	fclose(pattern_fp);

	/*Text loading*/
	text_fp = fopen("data/text", "r");
	
	for( j = 0; j < n; j++ )
		for( i = 0; i < n; i++ ) {
			c = fgetc (text_fp);
			/* If we encounter '\n' pull the next character*/
			if( c != 10 )
				text[j][i] = c;
			else
				text[j][i] = fgetc( text_fp );
		}
	
	fclose(text_fp);
}

/*Loads text and pattern into tables*/
void create_files(int pattern_size, int text_size, int alphabet)
{
	FILE *text_fp, *pattern_fp;
	
	unsigned int i,j;
	
	/* initialize random seed: */
	srand( time( NULL ) );
	
	/*Creating pattern*/
	pattern_fp = fopen("data/pattern", "w");
	
	for( j = 0; j < pattern_size; j++ ) {
		for( i = 0; i < pattern_size; i++ )
			fputc( rand() % alphabet + 1, pattern_fp );
		fputc( 10, pattern_fp );
	}
		
	fclose( pattern_fp );

	/*Creating text*/
	text_fp = fopen("data/text", "w");
	
	for( j = 0; j < text_size; j++ ){
		for( i = 0; i < text_size; i++ )
			fputc ( rand() % alphabet + 1, text_fp );
		fputc ( 10, text_fp );
	}
	
	fclose( text_fp );
}

