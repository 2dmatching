#include "code.h"

int calculate_actual_match(char **text, char **pattern, int m, int n, int x, int y)
{
	unsigned int i = 0, k, j, l, hit = 0;

	for( l = y; l < m + y; l++, i++ ) {

		k = 0;
		
		for( j = x; j < m + x; j++, k++ )
			if( pattern[i][k] == text[l][j] )
				hit++;
	}
	
	if ( hit == m*m )
		return 1;
	return 0;
}

unsigned int karp(char **text, char **pattern, int m, int n)
{
	unsigned int i, j;

	double Bm = 1;
	
	#define B 128

	unsigned int row, matches = 0;

	double pattern_hash = 0, text_hash = 0;

	double vertical_pattern, vertical_text, vertical_previous_text;

	/*Compute the hash for the pattern*/
	for( i = 0; i < m; i++ ) {

		Bm *= B;

		vertical_pattern = 0;

		/*Sum m characters in the same row*/
		for( j = 0; j < m; j++ )
			vertical_pattern += pattern[j][i];
		
		pattern_hash = pattern_hash * B + vertical_pattern;
	}

	/*Compute the hash for the text*/
	for( row = 0; row < n - m + 1; row++ ) {
		
		text_hash = 0;

		/*Compute the hash for the first m elements of each row of the text*/
		for( i = 0; i < m; i++ ) {

			vertical_text = 0;
			
			/*Sum m characters in the same row*/
			for( j = 0 + row; j < m + row; j++ )
				vertical_text = vertical_text + text[j][i];

			text_hash = text_hash * B + vertical_text;
		}

		/*Compute the hash for the rest n-m elements of each row of the text*/
		for( i = m; i < n; i++ ) {
		
			/*Check if the hashes actually match*/
			if( pattern_hash == text_hash && calculate_actual_match( text, pattern, m, n, i - m, row ))
				matches++;

			/* Update text's hash*/
			vertical_text = 0;					
			vertical_previous_text = 0;

			/*Sum m characters from i-m (vpt) and from i (vt) */
			for( j = 0 + row; j < m + row; j++ ) {
				vertical_previous_text = vertical_previous_text + text[j][i-m];
				vertical_text = vertical_text + text[j][i];
			}
			
			text_hash = ( text_hash * B ) - ( vertical_previous_text * Bm ) + vertical_text;
		}
	}
	return matches;
}

