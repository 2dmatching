#include "code.h"

int naive3( char **text, char **pattern, int m, int n, int textrow, int textcolumn, int patternrow )
{
	unsigned int i, j, k;
	
	for ( i = textcolumn; i < n - m + 1; i++ ) {
	
		k = i;
		
		for ( j = 0; j < m; j++ ) {
			
			if ( pattern[patternrow][j] == text[textrow][k] )
				k++;
			else
				break;
		}
		if ( j == m )
			return k-m;
		else
			j = 0;
	}
	/* No match found on this row */
	return -1;
}

int naive3b( char **text, char **pattern, int m, int n, int textrow, int textcolumn, int patternrow )
{
	unsigned int j;
	
	for ( j = 0; j < m; j++ ) {
		
		if ( pattern[patternrow][j] == text[textrow][textcolumn] )
			textcolumn++;
		else
			return -1;
	}
	/* If reached through here, report success */
	return 0;
}

unsigned int bird2( char **text, char **pattern, int m, int n )
{	
	/* x and y can be -1 */
	int x;
	
	unsigned int i, row, matches = 0;
	
	/* Iterate rows */
	for ( row = 0; row < n - m + 1; row++ ) {
	
		x = 0;
		
		/* Iterate through results on the same row */
		while ( x != -1 ) {

			x = naive3 ( text, pattern, m, n, row, x, 0 );
			
			/* If a row is found, check the m - 1 rows underneath it for matches */
			if ( x != -1 ) {

				for ( i = 1; i < m; i++) {
					/* Optimization: instead of checking against n chars on
					 subsequent rows, just check against the next m chars after x without sliding the window */
 					if ( naive3b ( text, pattern, m , n, row + i, x, i ) == -1 )
						break;
				}
				
				if ( i == m )
					matches++;
				/* After each attempt,increase the counter +1 */
				x++;
			}
		}
	}
	return matches;
}
