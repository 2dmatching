#include "code.h"

unsigned int baeza( char **text, char **pattern, int m, int n )
{
	unsigned int i, j, row, matches = 0;
	
	int x, y;
	
	/* Iterate every m rows (first check lines 0 to m - 1) */
	for ( row = m - 1; row < n; row += m ) {

		/* Search the same row for occurences of the different lines of the pattern */
		for ( i = 0; i < m; i++ ) {
			
			/* In each row start searching from the begining */
			x = 0;
		
			while ( x != -1) {

				/* Check text's row against each pattern's row */
				x = aho( text, pattern, m, n, row, x, i, n );
				
				/* If a row is not found procceed to the next row */
				if ( x != -1 ) {	

					/* If there is a match at i = 3, check vertically the text above for i = 0-2 and below for 4-m*/
							
					/* Check lines above the current */
					for ( j = 0; j < i; j++ ) {
						y = aho( text, pattern, m, n, row - i + j, x, j, x + m );

						if ( y != x)
							goto newline;
					}
						
					/* Check lines below the current */
					for ( j = 1; j < m - i; j++ ) {

						/* Avoid crashing when x = n or n-1 */
						if ( row + j >= n )
							goto newline;
						
						y = aho( text, pattern, m, n, row + j, x, i + j, x + m );

						if ( y != x )
							goto newline;
					}
			
					/* If we reached here we should have a match */
					matches++;

				/* If a mismatch is found, don't bother searching for the other y lines */
				newline:

					/* Now increase counter to continue search in the same row */
					x++;
				}
				else break;
			}
		}
	}
	return matches;
}
