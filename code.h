#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <mpi/mpi.h>

extern void load_files( char **text, char **pattern, int m, int n );

extern void print_pattern( char **text, char **pattern, int m, int n );

extern void create_files(int pattern_size, int text_size, int alphabet);

extern int power(int number, int power);

extern unsigned int naive(char **text, char **pattern, int m, int n);

extern unsigned int karp(char **text, char **pattern, int m, int n);

extern unsigned int zhu( char **text, char **pattern, int m, int n, int version );

extern unsigned int bird( char **text, char **pattern, int m, int n );

extern unsigned int bird2( char **text, char **pattern, int m, int n );

extern unsigned int baeza( char **text, char **pattern, int m, int n );

extern unsigned int baeza2( char **text, char **pattern, int m, int n );

extern unsigned int search_kmp( double *text2, double *pattern2, int m, int n );

extern unsigned int search_bm( double *text2, double *pattern2, int m, int n );

extern int aho( char **text, char **pattern, int m, int n, int trow, int tcolumn, int prow, int tlength );

#define Q 16647133 /* A big prime number for the Zhu algorithm*/

/*Don't forget to change MAXCHAR on aho.c */

#define ALPHABET_SIZE 2

