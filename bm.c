#include "code.h"

void heur1( int m, unsigned int *CharJump )
{
	unsigned int i;
	
	for ( i = 0; i < m; i++ )
		CharJump[i] = m - i -1;
}

void heur2( double *pattern2, int m, int n, unsigned int *MatchJump, unsigned int *BackUp )
{
	unsigned int i, ia, ib;
	
	for ( i = 1; i < m; i++ )
		MatchJump[i] = 2 * m - i;
		
	i = n;
	ia = n + 1;
	
	while ( i > 0 ) {
		
		BackUp[i] = ia;
		
		while ( ia <= m && pattern2[i - 1] != pattern2[i -1] ) {

			if ( MatchJump[ia] > m - i )
				MatchJump[ia] = m - i;
			ia = BackUp[ia];
		}
		i--;
		ia--;
	}
	
	for ( i = 1; i < ia; i++) {

		if ( MatchJump[i] > m + ia - i )
			MatchJump[i] = m + ia - i;
	}
			
	ib = BackUp[ia];
	
	while ( ia <= m ) {
		while ( ia <= ib ) {

			if ( MatchJump[ia] > ib - ia + m )
				MatchJump[ia] = ib - ia + m;
			ia++;
		}
		ib = BackUp[ib];
	}
}

int jump( double *text2, double *pattern2, int m, int mark, unsigned int *CharJump )
{
	unsigned int i;

	for ( i = 0; i < m; i++ )
		if( text2[mark] ==  pattern2[i] )
			return CharJump[i];
	return m;	
}

unsigned int search_bm( double *text2, double *pattern2, int m, int n )
{
	#ifndef max
	#define max(a,b) ((a) > (b) ? (a) : (b))
	#endif
	
	unsigned int *CharJump = ( unsigned int* ) malloc( m * sizeof( unsigned int* ));
	
	if( CharJump == NULL ) {
		printf("Failed to allocate array!\n");
		exit(1);
	}
	
	unsigned int *BackUp = ( unsigned int* ) malloc( n * sizeof( unsigned int* ));
	
	if( BackUp == NULL ) {
		printf("Failed to allocate array!\n");
		exit(1);
	}
	
	unsigned int *MatchJump = ( unsigned int* ) malloc ( 2 * sizeof ( unsigned int* ) * ( m + 1 ) );
	
	if( MatchJump == NULL ) {
		printf("Failed to allocate array!\n");
		exit(1);
	}

	unsigned int matches = 0;
	
	unsigned int ia, ib;

	unsigned int pattern_marker, text_marker;
	
	pattern_marker = m;
	
	text_marker = m -1;

	/* bm uses two heuristics */
	heur1( m, CharJump );
	
	heur2( pattern2, m, n, MatchJump, BackUp );	

	const int limit = n * n - ( n * ( m - 1 ) );
	
	while ( text_marker < limit )
	{
		/* We have a partial match */
		if ( text2[text_marker] == pattern2[pattern_marker -1] ) {
			pattern_marker--;
			text_marker--;
		}

		else {
			ia = jump( text2, pattern2, m, text_marker, CharJump );
			ib = MatchJump[pattern_marker];
			text_marker += max( ia, ib );
			pattern_marker = m;
		}
		
		/* We have a match */	
		if ( pattern_marker == 0 ) {
			matches++;
			text_marker += m + 1;	
			pattern_marker = m;
		}
	}
	
	free( CharJump );
	free( BackUp );
	free( MatchJump );

	return matches;
}

